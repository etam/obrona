all: main.pdf

main.pdf:

%.pdf: %.tex always_remake
	latexmk -pdf $<


clean:
	latexmk -c

clean-all: clean-img
	latexmk -C


.PHONY: all clean clean-all always_remake
